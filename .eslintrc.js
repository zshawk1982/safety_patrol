/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-02 16:17:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\.eslintrc.js
 */
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: ['plugin:vue/essential', 'eslint:recommended'],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'camelcase': 0,
    'space-before-function-paren': 0,
    'indent': 0,
    'no-callback-literal': 0,
    'no-unused-vars': 0,
    'no-useless-escape': 0
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  }

}
