import Mock from 'mockjs'

const List = []
const count = 80
for(let i = 0 ; i < count ; i++){
  List.push(Mock.mock({
    id: '@increment',
    name: '@ctitle(5, 10)',                       // 企业名称
    abridge:'@ctitle(2,4)',                       // 内部简称
    'type|1': ['xxx','ccc','jjj'],                // 类型
    'nature|1': ['互联网','房地产'],            // 性质
    secure: '@cname',                             // 安全负责人   
    phone: /^1(5|3|7|8)[0-9]{9}$/,                // 联系电话
    'regAddress|1':['@province','@city','@city'], // 注册地址
    comAddress:'@province',                       // 企业地址
    desc:'@cparagraph',                           // 主营业务
    username:'administrator',                     // 系统登陆名
    remarks:''                                    // 备注
  }))
}

export default [
  {
    url: '/vue-element-admin/enterprise/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/vue-element-admin/enterprise/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const message of List) {
        if (message.id === +id) {
          return {
            code: 20000,
            data: message
          }
        }
      }
    }
  },
  {
    url: '/vue-element-admin/enterprise/delete',
    type: 'get',
    response: config => {
      const { userId } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },
]

