/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-10 15:52:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\user.js
 */
import Mock from 'mockjs'

let checktargetData = [

  {
    id: 1,
    // pid: 1,
    label: '锅炉',
    comments: '锅炉'
  },
  {
    id: 2,
    label: '安全检查',
    pid: 1,
    comments: '安全检查',
    children: [
      {
        id: 9,
        pid: 2,
        label: '制度检查',
        comments: '制度检查',
      },
      {
        id: 10,
        pid: 2,
        label: '作业安全',
        comments: '作业安全',
      },
      {
        id: 11,
        pid: 2,
        label: '安全标识',
        comments: '作业安全',
      }
    ]
  },
  {
    id: 3,
    // pid: 1,
    label: '生产线',
    comments: '生产线',

  },
  {
    id: 4,
    // pid: 1,
    label: '烟囱',
    comments: '烟囱',

  }

]
export default [

  //获取组织树列表
  {
    url: '/vue-element-admin/checktarget/treeData',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          checktargetData
        }
      }
    }
  },
  // 删除组织
  {
    url: '/vue-element-admin/checktarget/delete',
    type: 'get',
    response: config => {
      const { userId } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  //新增组织
  {
    url: '/vue-element-admin/checktarget/add',
    type: 'get',
    response: config => {
      const { checktargetName, pid } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },


]
