import Mock from 'mockjs'

const List = []
const count = 30


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    planName: '@ctitle(5, 20)', //计划名称
    target: '@ctitle(4,5)', //目标
    'unit|1': ['重庆某某化工厂', '重庆XX工厂','重庆XX石工厂'], //单位
    'state|1': ['未检查', '已检查'], //任务状态
    'planType|1': ['周期', '单次'],//计划类型
    beginTime: '@datetime("yyyy-MM-dd HH:mm:ss")',//开始时间
    endTime: '@datetime("yyyy-MM-dd HH:mm:ss")',//结束时间
  }))
}

export default [
  {
    url: '/vue-element-admin/task/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/vue-element-admin/task/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const task of List) {
        if (task.id === +id) {
          return {
            code: 20000,
            data: task
          }
        }
      }
    }
  },

  {
    url: '/vue-element-admin/task/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/vue-element-admin/task/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/vue-element-admin/task/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

