/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-15 13:31:45
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\user.js
 */
import Mock from 'mockjs'


const List = []
const count = 30


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    'troubleName|1': ['在XX检查中锅炉整改意见2', '在XX检查中锅炉整改意见3', '在XX检查中锅炉整改意见1'],
    'companyName|1': ['万达集团', '沃尔玛中国有限公司', '重庆工程学院'],
    'grade|1': [1, 2], //1表示一般 2表示紧急
    'status|1': [0, 1, 2], //0 未整改，1已整改 2超期
    findDate: '@datetime("yyyy-MM-dd HH:mm:ss")',
    expirationDate: '@datetime("yyyy-MM-dd HH:mm:ss")',
  }))
}


export default [


  // get trouble info 获取指定隐患信息
  {
    url: '/vue-element-admin/trouble/info',
    type: 'get',
    response: config => {
      const { id } = config.queryi
      const info = List[0]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: '查询隐患详情失败'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  //获取隐患列表
  {
    url: '/vue-element-admin/troubles/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },



]
