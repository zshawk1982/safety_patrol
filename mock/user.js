/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-08 15:03:54
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\user.js
 */
import Mock from 'mockjs'
const tokens = {
  admin: {
    token: 'admin-token'
  },
  enterpriseUser: {
    token: 'enterpriseUser-token'
  }
}

const List = []
const count = 30

//头像图片
const avatar_image_uri = 'https://wpimg.wallstcn.com/e4558086-631c-425c-9430-56ffb46e70b3'

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    loginDate: '@datetime("yyyy-MM-dd HH:mm:ss")',
    registerDate: '@datetime("yyyy-MM-dd HH:mm:ss")',
    username: '@first',
    realname: '@cname',
    telephone: /^1(5|3|7|8)[0-9]{9}$/,
    email: '@email',
    qq: /^6(5|3|7|8)[0-9]{9}$/,
    'organization|1': ["机构办公室1", "机构办公室2"],
    'post|1': ['1', "2"],
    'work|1': ['0', '1', '2'],
    'role|1': ['admin', 'enterpriseUser'],
    avatar_image_uri
  }))
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: '我是超级管理员',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'enterpriseUser-token': {
    roles: ['enterpriseUser'],
    introduction: '我是企业用户',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal enterpriseUser'
  }
}

export default [
  // user login
  {
    url: '/vue-element-admin/user/login',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          code: 60204,
          message: 'Account and password are incorrect.'
        }
      }

      return {
        code: 20000,
        data: token
      }
    }
  },

  // get user info 获取当前用户信息
  {
    url: '/vue-element-admin/user/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  // user logout
  {
    url: '/vue-element-admin/user/logout',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  //获取用户列表
  {
    url: '/vue-element-admin/user/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  // 删除用户
  {
    url: '/vue-element-admin/user/delete',
    type: 'get',
    response: config => {
      const { userId } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },


]
