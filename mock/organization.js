/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-10 15:52:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\user.js
 */
import Mock from 'mockjs'

let organizationData = [
  {
    id: 1,
    label: '系统机构',
    comments: '系统机构',
    children: [
      {
        id: 4,
        label: '安全管理局1',
        pid: 1,
        comments: '安全管理局1',
        children: [
          {
            id: 9,
            pid: 4,
            label: '办公室1',
            comments: '办公室1',
            children: [
              {
                id: 10,
                pid: 9,
                label: '第一小组',
                comments: '第一小组',
              },
              {
                id: 11,
                pid: 9,
                label: '第二小组',
                comments: '第二小组',
              }
            ]
          },
          {
            id: 10,
            pid: 4,
            label: '办公室2',
            comments: '办公室2',
          }
        ]
      },
      {
        id: 2,
        pid: 1,
        label: '安全管理局2',
        comments: '安全管理局2',
        children: [
          {
            id: 5,
            pid: 2,
            label: '办公室21',
            comments: '办公室21',
          },
          {
            id: 6,
            pid: 2,
            label: '办公室22',
            comments: '办公室22',
          }
        ]
      }
    ]
  },

]
export default [

  //获取组织树列表
  {
    url: '/vue-element-admin/organization/treeData',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          organizationData
        }
      }
    }
  },
  // 删除组织
  {
    url: '/vue-element-admin/organization/delete',
    type: 'get',
    response: config => {
      const { userId } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  //新增组织
  {
    url: '/vue-element-admin/organization/add',
    type: 'get',
    response: config => {
      const { organizationName, pid } = config.query
      return {
        code: 20000,
        data: 'success'
      }
    }
  },


]
