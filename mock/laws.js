import Mock from 'mockjs'

const List = []
const count = 130

for (let i = 0; i < count; i++) {
    List.push(Mock.mock({
      id: '@increment',     //序号
      name: '@ctitle(5,10)',
      createDate: '@datetime("yyyy-MM-dd HH:mm:ss")',//创建时间||发布时间
      type:"@ctitle(2, 5)",//种类 lower 小写字母
      issueUnit:"@ctitle(5, 10)",//发布机关
      issuedNumber:"@ctitle(5, 10)",//发文字号
      effectiveDate: '@datetime("yyyy-MM-dd HH:mm:ss")',//实施时间
      url:'http://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf'
    }))
  }
  export default [
    {
        url: '/vue-element-admin/laws/list',
        type: 'get',
        response: config => {
          const { importance, type, title, page = 1, limit = 20, sort } = config.query
    
          let mockList = List.filter(item => {
            if (importance && item.importance !== +importance) return false
            if (type && item.type !== type) return false
            if (title && item.title.indexOf(title) < 0) return false
            return true
          })
    
          if (sort === '-id') {
            mockList = mockList.reverse()
          }
    
          const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
    
          return {
            code: 20000,
            data: {
              total: mockList.length,
              items: pageList
            }
          }
        }
      },
      {
        url: '/vue-element-admin/laws/detail',
        type: 'get',
        response: config => {
          const { id } = config.query
          for (const message of List) {
            if (message.id === +id) {
              return {
                code: 20000,
                data: message
              }
            }
          }
        }
      },
      {
          url: '/vue-element-admin/laws/delete',
          type: 'get',
          response: config => {
            const { userId } = config.query
            return {
              code: 20000,
              data: 'success'
            }
          }
      }
  ]