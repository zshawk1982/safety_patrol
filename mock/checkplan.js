import Mock from 'mockjs'

const List = []
const count = 30


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    planName: '@ctitle(5, 20)', //计划名称
    people: '@cname',//执行人
    'checkState|1': ['执行中', '未开始', '暂停执行', '执行结束'],//执行状态
    'unit|1': ['重庆某某化工厂', '重庆XX工厂', '重庆XX石工厂'], //单位
    'type|1': ['单次', '间隔20日'],//类型
    beginTime: '@datetime("yyyy-MM-dd HH:mm:ss")',//开始时间
    endTime: '@datetime("yyyy-MM-dd HH:mm:ss")',//结束时间
    objectList: [
      {
        id:1,
        unit: "某某单位",
        checkObj: "锅炉",
        checkTable: "某某表",
        executePeop: "某某人"
      }, {
        id:2,
        unit: "某某单位",
        checkObj: "锅炉",
        checkTable: "某某表",
        executePeop: "某某人"
      }
    ]
  }))
}

export default [
  {
    url: '/vue-element-admin/checkplan/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  
  {
    url: '/vue-element-admin/checkplan/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const checkplan of List) {
        if (checkplan.id === +id) {
          return {
            code: 20000,
            data: checkplan
          }
        }
      }
    }
  },

  {
    url: '/vue-element-admin/checkplan/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/vue-element-admin/checkplan/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/vue-element-admin/checkplan/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

