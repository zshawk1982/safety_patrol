/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-06 20:25:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\role\routes.js
 */
// Just a mock data

export const constantRoutes = [
  {
    path: '/redirect',
    component: 'layout/Layout',
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: 'views/redirect/index'
      }
    ]
  },
  {
    path: '/login',
    component: 'views/login/index',
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: 'views/login/auth-redirect',
    hidden: true
  },
  {
    path: '/404',
    component: 'views/error-page/404',
    hidden: true
  },
  {
    path: '/401',
    component: 'views/error-page/401',
    hidden: true
  },
  {
    path: '/',
    component: 'layout/Layout',
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: 'views/dashboard/index',
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  }
]

export const asyncRoutes = [
  {
    path: '/system',
    component: 'layout/Layout',
    redirect: '/system/person/index',
    name: 'System',
    meta: {
      title: '系统管理',
      icon: 'nested',
      roles: ['admin']
    },
    children: [
      {
        path: 'person',
        component: 'views/system/person/index', // Parent router-view
        name: 'Person',
        meta: { title: '用户管理' },
      },
      {
        path: 'role',
        component: 'views/system/role/index', // Parent router-view
        name: 'Role',
        meta: { title: '角色管理' },
      },
      {
        path: 'organization',
        name: 'Organization',
        component: 'views/system/organization/index',
        meta: { title: '组织机构管理' }
      }
    ]
  },
  {
    path: '/permission',
    component: 'layout/Layout',
    redirect: '/permission/page',
    alwaysShow: true, // will always show the root menu
    name: 'Permission',
    meta: {
      title: 'Permission',
      icon: 'lock',
      roles: ['admin', 'enterpriseUser'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: 'views/permission/page',
        name: 'PagePermission',
        meta: {
          title: 'Page Permission',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'directive',
        component: 'views/permission/directive',
        name: 'DirectivePermission',
        meta: {
          title: 'Directive Permission'
          // if do not set roles, means: this page does not require permission
        }
      },
      {
        path: 'role',
        component: 'views/permission/role',
        name: 'RolePermission',
        meta: {
          title: 'Role Permission',
          roles: ['admin']
        }
      }
    ]
  },
  {
    path: '/message',
    component: 'layout/Layout',
    redirect: '/message/list',
    name: 'Messgage',
    meta: {
      title: '消息中心',
      icon: 'example'
    },
    children: [
      {
        path: 'create',
        component: 'views/message/create',
        name: 'CreateMessage',
        meta: { title: '消息发布', icon: 'edit', roles: ['admin'] },
      },
      {
        path: 'edit/:id(\\d+)',
        component: 'views/message/edit',
        name: 'EditMessage',
        meta: { title: '编辑消息', noCache: true, activeMenu: '/message/list' },
        hidden: true
      },
      {
        path: 'messageDetail/:id(\\d+)',
        component: 'views/message/messageDetail',
        name: 'DetailMessage',
        meta: { title: '消息详情', noCache: true, activeMenu: '/message/list' },
        hidden: true
      },
      {
        path: 'list',
        component: 'views/message/list',
        name: 'MessgaeList',
        meta: { title: '消息中心', icon: 'list' }
      }
    ]
  },
  {
    path: '/laws',
    component: 'layout/Layout',
    redirect: '/laws/list',
    name: 'Laws',
    meta: {
      title: '法律法规库',
      icon: 'example'
    },
    children: [
      {
        path: 'create',
        component: 'views/laws/create',
        name: 'CreateLaws',
        meta: { title: '新增法规', icon: 'edit', roles: ['admin'] },
      },
      {
        path: 'edit/:id(\\d+)',
        component: 'views/laws/edit',
        name: 'EditLaws',
        meta: { title: '编辑法规', noCache: true, activeMenu: '/laws/list' },
        hidden: true
      },
      {
        path: 'list',
        component: 'views/laws/list',
        name: 'LawsList',
        meta: { title: '法规列表', icon: 'list' }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

export const enterpriseRoutes = [
  {
    path: 'create',
    component: 'views/message/create',
    name: 'CreateMessage',
    meta: { title: '消息发布', icon: 'edit', roles: ['admin'] },
  }
]
