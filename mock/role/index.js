/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-11 12:27:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\mock\role\index.js
 */
import Mock from 'mockjs'
import { deepClone } from '../../src/utils/index.js'
import { constantRoutes } from './routes.js'

// const routes = deepClone([...constantRoutes, ...asyncRoutes])
// const mconstantRoutes = require("../data/mconstant.json")
const adminRoutes = require("../data/admin.json")
const enterpriseUserRoutes = require("../data/enterpriseUser.json")
const routes = deepClone([...constantRoutes, ...adminRoutes])
// 系统管理员
// 任务计划人
// 企业安全负责人
// 安检专家
const roles = [
  {
    key: 'admin',
    name: '管理员',
    description: '超级管理员，能够看到所有页面',
    routes: routes
  },
  {
    key: 'enterpirseUser',
    name: '企业用户',
    description: '企业用户，只能看见法律法规库和消息中心',
    routes: deepClone([...constantRoutes, ...enterpriseUserRoutes])
    // routes: enterpriseRoutes// just a mock
  },
  // {
  //   key: 'visitor',
  //   name: '访客',
  //   description: '访客，只能看见首页',
  //   routes: [{
  //     path: '',
  //     redirect: 'dashboard',
  //     children: [
  //       {
  //         path: 'dashboard',
  //         name: 'Dashboard',
  //         meta: { title: 'dashboard', icon: 'dashboard' }
  //       }
  //     ]
  //   }]
  // }
]

export default [
  // mock get all routes form server
  {
    url: '/vue-element-admin/routes',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: routes
      }
    }
  },

  // mock get all roles form server
  {
    url: '/vue-element-admin/roles',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: roles
      }
    }
  },

  // add role
  {
    url: '/vue-element-admin/role',
    type: 'post',
    response: {
      code: 20000,
      data: {
        key: Mock.mock('@integer(300, 5000)')
      }
    }
  },

  // update role
  {
    url: '/vue-element-admin/role/[A-Za-z0-9]',
    type: 'put',
    response: {
      code: 20000,
      data: {
        status: 'success'
      }
    }
  },

  // delete role
  {
    url: '/vue-element-admin/role/[A-Za-z0-9]',
    type: 'delete',
    response: {
      code: 20000,
      data: {
        status: 'success'
      }
    }
  },
  //获取指定角色的路由列表
  {
    url: '/vue-element-admin/roleRoute',

    type: 'get',
    response: config => {
      console.log('%%%%%%%%%%%%%%%%%%%', config)
      const { role } = config.query
      // mock error
      if (!role) {
        return {
          code: 60204,
          message: '该角色没有相应权限'
        }
      }
      if (role === 'admin') {
        return {
          code: 20000,
          //url对应path，perms对应component
          data: {
            menuList: adminRoutes
          }
        }
      } else {
        return {
          code: 20000,
          data: {
            menuList: enterpriseUserRoutes
          }
        }
      }
    }

  },
]
