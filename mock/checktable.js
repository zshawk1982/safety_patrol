import Mock from 'mockjs'

const List = []
const count = 30


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    target: '@ctitle(2, 6)', //检查目标
    tableName: '@ctitle(5, 20)', //检查表名称
    'state|1': ['停用', '启用'],//状态
    changeTime: '@datetime("yyyy-MM-dd HH:mm:ss")', //使用状态改变时
    checktableData:[

      {
        id: 1,
        // pid: 1,
        label: '锅炉安全管理基本要求（40）',
        comments: '锅炉安全管理基本要求（40）'
      },
      {
        id: 2,
        label: '锅炉安全管理基本要求（40）',
        // pid: 1,
        comments: '锅炉安全管理基本要求（40）',
        children: [
          {
            id: 1,
            pid: 2,
            label: '岗位安全责任制（20）',
            comments: '岗位安全责任制（20）',
          },
          {
            id: 2,
            pid: 2,
            label: '安全管理制度（10）',
            comments: '安全管理制度（10）',
          },
          {
            id: 3,
            pid: 2,
            label: '锅炉及辅助设备的操作规程（10）',
            comments: '锅炉及辅助设备的操作规程（10）',
          }
        ]
      },
      {
        id: 3,
        label: '锅炉安全附件的检查（40）',
        // pid: 1,
        comments: '锅炉安全附件的检查（40）',
        children: [
          {
            id: 1,
            pid: 3,
            label: '安全阀（20）',
            comments: '安全阀（20）',
          },
          {
            id: 2,
            pid: 3,
            label: '水位表（20）',
            comments: '水位表（20）',
          }
        ]
      }
    ]
  }))
}

export default [
  {
    url: '/vue-element-admin/checktable/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/vue-element-admin/checktable/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const checktable of List) {
        if (checktable.id === +id) {
          return {
            code: 20000,
            data: checktable
          }
        }
      }
    }
  },

  {
    url: '/vue-element-admin/checktable/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/vue-element-admin/checktable/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/vue-element-admin/checktable/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

