import Mock from 'mockjs'

const List = []
const count = 130

const image_uri = 'https://wpimg.wallstcn.com/e4558086-631c-425c-9430-56ffb46e70b3'

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    createDate: '@datetime("yyyy-MM-dd HH:mm:ss")',//创建时间||发布时间
    userName:'@cname',//发件人
    'department|1':['开发部', '人事部','财务部'],//部门
    title: '@ctitle(5, 20)',//标题
    'type|1': ['新消息', '已阅读','已回复'],//类别
    platforms: ['a-platform'],
    image_uri,
    messCount:'@csentence',//消息内容
    messageRemark:'@csentence',//备注
    overTime:'@datetime("yyyy-MM-dd HH:mm:ss")',//完成时间
    'grade|1': ['一级', '二级'],//级别
    'genre|1':['1', '2'],//1 消息 2 任务
    revert:[{//回复数组
      processor:'@cname',//处理人
      proTime:'@datetime("yyyy-MM-dd HH:mm:ss")',//处理时间
      transpond:'@cname',//转发对象
      content:'@csentence',//内容
    }]
  }))
}

export default [
  {
    url: '/vue-element-admin/message/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/vue-element-admin/message/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const message of List) {
        if (message.id === +id) {
          return {
            code: 20000,
            data: message
          }
        }
      }
    }
  },

  {
    url: '/vue-element-admin/message/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/vue-element-admin/message/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/vue-element-admin/message/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

