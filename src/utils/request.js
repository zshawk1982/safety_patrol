/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-06-22 18:21:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\utils\request.js
 */
import axios from 'axios'
import qs from 'qs'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // baseURL: 'http://106.15.182.96:8081', // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
  // paramsSerializer: function (params) {
  //   return qs.stringify(params, { indices: false })
  // },
})
service.defaults.transformRequest = (data, config) => {
  console.log(data, config, 'test request')
  // 在向服务器发送前，修改请求数据
  if (!config['Content-Type'])
    return qs.stringify(data, { indices: false });
  // console.log('data---------', qs.stringify(data, { indices: false }))
  // return qs.stringify(data, { arrayFormat: 'indices', allowDots: true })
  switch (config['Content-Type'].toLowerCase()) {
    case 'application/json': {
      //将javascript对象格式的请求数据转换为JSON格式再发送到服务器
      return JSON.stringify(data)
    }
    case 'multipart/form-data': {
      //返回表单数据formData的键值对，
      // console.log('data---------', data)
      return data.formData
    }
    default: {
      return qs.stringify(data, { indices: false })
      // return qs.stringify(data, { arrayFormat: 'indices', allowDots: true })
    }
  }
}


// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      console.log('token', getToken())
      config.headers['x-access-token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    // console.log(response, 'response...........')
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 20000) {
      // Message({
      //   message: res.message || res.msg || 'Error',
      //   type: 'error',
      //   duration: 5 * 1000
      // })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        // MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
        //   confirmButtonText: 'Re-Login',
        //   cancelButtonText: 'Cancel',
        //   type: 'warning'
        // }).then(() => {
        //   store.dispatch('user/resetToken').then(() => {
        //     location.reload()
        //   })
        // })
        MessageBox.confirm('你已经登陆过了，你可以选择留在该页面或者重新登录', '确认重新登录', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || res.msg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
