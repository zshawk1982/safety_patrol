import PDFJS from 'pdfjs-dist'
PDFJS.GlobalWorkerOptions.workerSrc = "pdfjs-dist/build/pdf.worker.js";
/*
    el:'装载PDF的容器'，
    fileSrc:'文件地址'，
    scale:'缩放倍数'
*/
export async function loadPdf({el,fileSrc,scale=2}) {
    let pdfCol = document.querySelector(el)
    let pdf = await PDFJS.getDocument(fileSrc)
    let pages = pdf.numPages
     for (let i = 1; i <= pages; i++) {
      let canvas = document.createElement('canvas')
      let page = await pdf.getPage(i)
      let viewport = page.getViewport(scale)
      let context = canvas.getContext('2d')
      canvas.height = viewport.height
      canvas.width = viewport.width
      let renderContext = {
          canvasContext: context,
          viewport: viewport
      }
      await page.render(renderContext)
      canvas.className = 'canvas'
      pdfCol.appendChild(canvas)
     }
}