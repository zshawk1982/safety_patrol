/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-07-04 07:35:36
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\store\modules\permission.js
 */
import { asyncRoutes, constantRoutes } from '@/router'
import { getAuthMenu } from '@/api/role'
import Layout from '@/layout'
/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
// function hasPermission(roles, route) {
//   if (route.meta && route.meta.roles) {
//     return roles.some(role => route.meta.roles.includes(role))
//   } else {
//     return true
//   }
// }

/**
 * 后台查询的菜单数据拼装成路由格式的数据
 * @param routes
 */
export function generaMenu(routes, data) {

  data.forEach(item => {
    // alert(JSON.stringify(item))
    console.log('item-------------', item)
    const menu = {
      path: item.functionUrl,
      component: item.component === '#' ? Layout : (resolve) => require([`@/views${item.component}`], resolve),
      // () => import(`@/views${item.component}`),
      hidden: item.hidden,
      redirect: item.redirect,
      children: [],
      name: 'menu_' + item.id,
      meta: { title: item.functionName, id: item.id, icon: item.icon }
    }
    if (item.children) {
      generaMenu(menu.children, item.children)
    }
    routes.push(menu)
  })
}




/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
// export function filterAsyncRoutes(routes, roles) {
//   const res = []

//   routes.forEach(route => {
//     const tmp = { ...route }
//     if (hasPermission(roles, tmp)) {
//       if (tmp.children) {
//         tmp.children = filterAsyncRoutes(tmp.children, roles)
//       }
//       res.push(tmp)
//     }
//   })

//   return res
// }

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  //生成可以访问的路由路径
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      const loadMenuData = []
      // if (roles.includes('admin')) {
      //   accessedRoutes = asyncRoutes || []
      // } else {
      //   //如果不是admin，就将asyncRoutes和roles里面的路由进行比较，得到最终可访问的路由
      //   // console.log('*************roles', roles)
      //   // console.log('*************asyncRoutes', asyncRoutes)
      //   accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      // }
      // 先查询后台并返回左侧菜单数据并把数据添加到路由
      getAuthMenu(roles).then(response => {
        // console.log('*****************', response.data)
        if (response.code !== 20000) {
          console.log('返回的数据有问题')
        } else {
          let data = response.data
          Object.assign(loadMenuData, data)
          generaMenu(asyncRoutes, loadMenuData)//生成左侧路由菜单
          let accessedRoutes = asyncRoutes
          //将可访问路径存储入state中
          commit('SET_ROUTES', accessedRoutes)
          //返回可访问路径给vue-router
          resolve(accessedRoutes)
        }
      }).catch(error => {
        console.log(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
