/*
 * @Author: your name
 * @Date: 2020-04-01 10:51:04
 * @LastEditTime: 2020-04-02 21:03:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\router\modules\systemRouter.js
 */
/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const systemRouter = {
  path: '/system',
  component: Layout,
  redirect: '/system/person/index',
  name: 'System',
  meta: {
    title: '系统管理',
    icon: 'nested',
    roles: ['admin']
  },
  children: [
    {
      path: 'person',
      component: () => import('@/views/system/person/index'), // Parent router-view
      name: 'Person',
      meta: { title: '用户管理' },
    },
    {
      path: 'role',
      component: () => import('@/views/system/role/index'), // Parent router-view
      name: 'Role',
      meta: { title: '角色管理' },
    },
    {
      path: 'organization',
      name: 'Organization',
      component: () => import('@/views/system/organization/index'),
      meta: { title: '组织机构管理' }
    }
  ]
}

export default systemRouter
