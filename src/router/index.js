import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
// import systemRouter from './modules/systemRouter'
/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/profile',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        hidden: true,
        meta: { title: '个人信息', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export let asyncRoutes = [
  // systemRouter,
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   alwaysShow: true, // will always show the root menu
  //   name: 'Permission',
  //   meta: {
  //     title: 'Permission',
  //     icon: 'lock',
  //     roles: ['admin'] // you can set roles in root nav
  //   },
  //   children: [
  //     {
  //       path: 'page',
  //       component: () => import('@/views/permission/page'),
  //       name: 'PagePermission',
  //       meta: {
  //         title: 'Page Permission',
  //         roles: ['admin'] // or you can only set roles in sub nav
  //       }
  //     },
  //     {
  //       path: 'directive',
  //       component: () => import('@/views/permission/directive'),
  //       name: 'DirectivePermission',
  //       meta: {
  //         title: 'Directive Permission'
  //         // if do not set roles, means: this page does not require permission
  //       }
  //     },
  //     {
  //       path: 'role',
  //       component: () => import('@/views/permission/role'),
  //       name: 'RolePermission',
  //       meta: {
  //         title: 'Role Permission',
  //         roles: ['admin']
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/message',
  //   component: Layout,
  //   redirect: '/message/list',
  //   name: 'Messgage',
  //   meta: {
  //     title: '消息中心',
  //     icon: 'message'
  //   },
  //   children: [
  //     {
  //       path: 'list',
  //       component: () => import('@/views/message/list'),
  //       name: 'ListMessage',
  //       meta: {
  //         title: '消息列表', icon: 'list',
  //         // roles: ['admin']
  //       },
  //     },
  //     {
  //       path: 'create',
  //       component: () => import('@/views/message/create'),
  //       name: 'CreateMessage',
  //       meta: {
  //         title: '消息发布', icon: 'guide',
  //         // roles: ['admin']
  //       },
  //     },
  //   ]
  // },
  // {
  //   path: '/laws',
  //   component: Layout,
  //   redirect: '/laws/list',
  //   name: 'Laws',
  //   meta: {
  //     title: '法律法规库',
  //     icon: 'example'
  //   },
  //   children: [
  //     {
  //       path: 'create',
  //       component: () => import('@/views/laws/create'),
  //       name: 'CreateLaws',
  //       meta: {
  //         title: '新增法规', icon: 'edit',
  //         // roles: ['admin']
  //       },
  //     },
  //     {
  //       path: 'edit/:id(\\d+)',
  //       component: () => import('@/views/laws/edit'),
  //       name: 'EditLaws',
  //       meta: { title: '编辑法规', noCache: true, activeMenu: '/laws/list' },
  //       hidden: true
  //     },
  //     {
  //       path: 'list',
  //       component: () => import('@/views/laws/list'),
  //       name: 'LawsList',
  //       meta: { title: '法规列表', icon: 'list' }
  //     }
  //   ]
  // },
  // {
  //   path: '/comcenter',
  //   component: Layout,
  //   redirect: '/comcenter',
  //   name: 'comcenter',
  //   meta:{
  //     title: '企业中心',
  //     icon: 'example'
  //   },
  //   children:[{
  //     path: '/cominfomanage',
  //     component: () => import('@/views/comCenter/comInfoManage/index'), // Parent router-view
  //     name: 'cominfomanage',
  //     meta: { title: '企业信息管理', icon: 'edit' },
  //     redirect: '/comcenter/cominfomanage',
  //     children: [{
  //       path: '/addCom',
  //       component: () => import('@/views/comCenter/comInfoManage/addCom'),
  //       name: 'addCom',
  //       meta: { title:'添加企业', icon: 'edit'},
  //     },{
  //       path: '/comInfo',
  //       component: () => import('@/views/comCenter/comInfoManage/comInfo'),
  //       name: 'comInfo',
  //       meta: { title:'企业信息', icon: 'edit' },
  //     }]
  //   }]
  // },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  asyncRoutes = []
  router.matcher = newRouter.matcher // reset router
}

export default router
