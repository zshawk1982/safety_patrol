/*
 * @Author: your name
 * @Date: 2020-04-23 08:13:22
 * @LastEditTime: 2020-05-19 12:24:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\task.js
 */
import request from '@/utils/request'

// 获取个人任务列表
export function fetchTaskList(query) {
  return request({
    url: '/api/check-task/getListByPaged', // 分页获取检查任务
    method: 'get',
    params: query
  })
}
// 检查完成保存
export function fetchSave(data) {
  return request({
    url: '/api/check-task/saveTaskCheckPaper',
    method: 'post',
    data

  })
}
//检查表检查
export function fetchTaskCheckForm(taskId) {
  return request({
    url: '/api/check-task/doTaskCheckPaper',
    method: 'get',
    params: {
      taskId
    }
  })
}

//无计划（开始检查）
// export function fetchNoPlan(noPlan) {
//   return request({
//     url: '/api/check-task/startCheckNoPlan',
//     method: 'get',
//     params: {
//       noPlan
//     }
//   })
// }
// 查看
export function fetchDetails(taskId) {
  return request({
    url: '/api/check-task/details',
    method: 'get',
    params: {
      taskId
    }
  })
}

export function updateTask(data) {
  return request({
    url: '/vue-element-admin/task/update',
    method: 'post',
    data
  })
}

//开始检查，获取任务信息
export function beginCheckInfo(taskId) {
  return request({
    url: '/api/check-task/doTaskStart',
    method: 'get',
    params: {
      taskId
    }
  })
}
//开始检查，保存开始检查结果
export function saveBeginCheck(data) {
  return request({
    url: '/api/check-task/saveTaskStart',
    method: 'post',
    data
  })
}

//结束检查，获取任务信息
export function endCheckInfo(taskId) {
  return request({
    url: '/api/check-task/doTaskEnd',
    method: 'get',
    params: {
      taskId
    }
  })
}
//获取草稿隐患信息
export function fetchDangerInfoBeforeDoTaskEnd(taskId) {
  return request({
    url: '/api/check-task/beforeDoTaskEnd',
    method: 'get',
    params: {
      taskId
    }
  })
}
//结束检查，保存结束检查结果
export function saveEndCheck(data) {
  return request({
    url: '/api/check-task/saveTaskEnd',
    method: 'post',
    data,
    headers: { "Content-Type": "multipart/form-data" }
  })
}
