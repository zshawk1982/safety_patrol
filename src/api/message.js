/*
 * @Author: your name
 * @Date: 2020-04-09 09:23:23
 * @LastEditTime: 2020-07-06 21:51:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\message.js
 */
import request from '@/utils/request'

export function fetchNewMsgList(data) {
  return request({
    url: '/api/message/getNewMsgList',
    method: 'post',
    data
  })
}
export function fetchReadMsgList(data) {
  return request({
    url: '/api/message/getReadMsgList',
    method: 'post',
    data
  })
}
export function fetchRepliedMsgList(data) {
  return request({
    url: '/api/message/getRepliedMsgList',
    method: 'post',
    data
  })
}
export function fetchMessage(msgId) {
  return request({
    url: '/api/message/getMessageDetail',
    method: 'get',
    params: { msgId }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/message/pv',
    method: 'get',
    params: { pv }
  })
}

export function addMessage(data) {
  return request({
    url: '/api/message/addMessage',
    method: 'post',
    data
  })
}
export function replyMessage(data) {
  return request({
    url: '/api/message/replyMessage',
    method: 'post',
    data
  })
}
export function forwardMessage(data) {
  return request({
    url: '/api/message/forwardMessage',
    method: 'post',
    data
  })
}


export function updateMessage(data) {
  return request({
    url: '/vue-element-admin/message/update',
    method: 'post',
    data
  })
}
