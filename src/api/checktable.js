/*
 * @Author: your name
 * @Date: 2020-04-29 19:33:27
 * @LastEditTime: 2020-05-05 17:23:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\checktable.js
 */
import request from '@/utils/request'

export function fetchList(query) { //获取检查表List
  return request({
    url: '/api/check-paper/selectCheckPaperListPage',
    method: 'get',
    params: query
  })
}
//check-paper/selectCheckPaperList 根据条件查询检查表
export function queryList(query) { //获取检查表List
  return request({
    url: '/api/check-paper/selectCheckPaperList',
    method: 'get',
    params: query
  })
}
export function fetchChecktable(id) {
  return request({
    url: '/vue-element-admin/checktable/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/checktable/pv',
    method: 'get',
    params: {
      pv
    }
  })
}

export function createChecktable(data) { //新增检查表
  return request({
    url: '/api/check-paper/addCheckPaper',
    method: 'post',
    data
  })
}
//删除检查表
export function deleteTable(data) {
  return request({
    url: '/api/check-paper/deleteCheckPaper',
    method: 'post',
    data
  })
}
//停用检查表
export function stopCheckTable(data) {
  return request({
    url: '/api/check-paper/DiscontinueUse',
    method: 'post',
    data
  })
}
//启用检查表
export function startCheckTable(data) {
  return request({
    url: '/api/check-paper/enableCheckPaper',
    method: 'post',
    data
  })
}
//预览检查表
export function lookTable(data) {
  return request({
    url: '/api/check-paper/getCheckIndicatorList',
    method: 'post',
    data
  })
}
//修改检查表
export function updatePaper(data) {
  return request({
    url: '/api/check-paper/updateCheckPaper',
    method: 'post',
    data
  })
}
