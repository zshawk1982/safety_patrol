/*
 * @Author: your name
 * @Date: 2020-04-19 19:09:34
 * @LastEditTime: 2020-05-15 08:55:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\laws.js
 */
import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/api/laws-regulations/getLawPage',
    method: 'get',
    params: query
  })
}
//获取法律法规详情
export function fetchLaw(lrId) {
  return request({
    url: '/api/laws-regulations/detail',
    method: 'post',
    data: { lrId }
  })
}
export function deleteLaw(data) {
  return request({
    url: '/api/laws-regulations/deleteLawRegulation',
    method: 'post',
    data
  })
}

export function addLaw(data) {
  return request({
    url: '/api/laws-regulations/addLawRegulation',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}

export function updateLaw(data) {
  return request({
    url: '/api/laws-regulations/updateLawRegulation',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}
