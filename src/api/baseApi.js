/*
 * @Author: your name
 * @Date: 2020-04-24 13:32:57
 * @LastEditTime: 2020-04-24 13:38:39
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\baseApi.js
 */
import request from '@/utils/request'
export function getBaseDicInfo(data) {
  // 1: 整改类型
  // 2: 隐患级别
  // 3: 隐患类别
  // 4: 隐患来源
  // 5: 复查结果
  // 6: 隐患状态
  // 7: 法规有效性
  // 8: 法规级别
  // 9: 专业类别
  // 10: 检查表适用对象
  return request({
    url: '/api/base-data-dictionary/getBaseDataByTypeId',
    // url: '/vue-element-admin/user/info',
    method: 'get',
    params: data
  })
}
