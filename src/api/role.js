/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-07-05 09:27:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\role.js
 */
import request from '@/utils/request'

//获取所有路由信息，管理员的和404，401的这些
export function getRoutes() {
  return request({
    // url: '/vue-element-admin/routes',
    // method: 'get'
    // url: '/api/sys/roleMenu',
    url: '/api/sys-function/getList',
    method: 'post'
  })
}

//获取所有的角色信息
export function getRoles(roleStatus) {
  return request({
    url: '/api/sys-role/getAllRoles',
    method: 'get',
    params: { roleStatus }
  })
}

//获取特定角色菜单信息
export function getRoleById(id) {
  return request({
    url: '/api/sys-role/getRoleById',
    method: 'get',
    params: { id }
  })
}

export function addRole(data) {
  return request({
    url: '/api/sys-role/create',
    method: 'post',
    data
  })
}

export function updateRole(data) {
  return request({
    url: `/api/sys-role/edit`,
    method: 'post',
    data
  })
}

//禁用角色
export function deleteRole(roleId) {
  return request({
    url: `/api/sys-role/stop`,
    method: 'post',
    data: { roleId }
  })
}
//启用角色
export function enableRole(roleId) {
  return request({
    url: `/api/sys-role/reuse`,
    method: 'post',
    data: { roleId }
  })
}

//根据角色获取角色菜单信息
export function getAuthMenu(roles) {
  // if (roles.includes('admin')) {
  //   console.log('############', roles)
  //   return request({
  //     url: `/vue-element-admin/roleRoute`,
  //     method: 'get',
  //     params: { role: 'admin' }
  //   })
  // } else {
  //   return request({
  //     url: `/vue-element-admin/roleRoute`,
  //     method: 'get',
  //     params: { role: 'enterpriseUser' }
  //   })
  // }
  return request({
    url: `/api/sys/roleMenu`,
    method: 'post',
  })

}
