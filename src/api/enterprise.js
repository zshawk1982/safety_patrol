/*
 * @Author: your name
 * @Date: 2020-04-19 19:09:34
 * @LastEditTime: 2020-06-10 12:33:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\enterprise.js
 */
import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/api/company/getCompanyPage',
    method: 'get',
    params: query
  })
}
export function fetchEnterpriseListByUser() {
  return request({
    url: '/api/company/getListByUser',
    method: 'get',
  })
}
//删除指定企业
export function deleteEnterprise(id) {
  return request({
    url: '/api/company/deleteCompany',
    method: 'get',
    params: {
      id: id
    }
  })
}
//新增企业
export function addEnterprise(data) {
  return request({
    url: '/api/company/addCompany',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}
//新增企业资质
export function addQualification(data) {
  return request({
    url: '/api/company-qualification/addCompanyQualification',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}
//新增企业用户
export function addEnterprisePeople(data) {
  return request({
    url: '/api/company_user/addCompanyUser',
    method: 'post',
    data,
  })
}
//编辑企业
export function editEnterprise(data) {
  return request({
    url: '/api/company/updateCompany',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}
//查看企业详细信息
export function fetchEnterpriseById(id) {
  return request({
    url: '/api/company/getCompany',
    method: 'get',
    params: {
      id
    },
  })
}
//查看指定企业的用户信息列表
export function fetchUserListByEnterpriseId(data) {
  return request({
    url: '/api/company_user/getCompanyUserList',
    method: 'post',
    data
  })
}
//查询指定企业资质信息列表
export function fetchQualificationListByEnterpriseId(data) {
  return request({
    url: '/api/company-qualification/listCompanyQualificationDetail',
    method: 'post',
    data
  })
}
//查询指定企业资质详情
export function fetchQualificationDetail(id) {
  return request({
    url: '/api/company-qualification/getCompanyQualificationDetailById',
    method: 'get',
    params: {
      id
    }
  })
}

//删除指定企业的用户
export function deleteEnterpriseUserById(id, companyId) {
  return request({
    url: '/api/company_user/deleteCompanyUser',
    method: 'get',
    params: {
      companyUserTypeId: id,
      companyId: companyId
    }
  })
}
//更新指定企业的用户
export function updateEnterpriseUser(data) {
  return request({
    url: '/api/company_user/updateCompanyUser',
    method: 'post',
    data
  })
}
//更新指定企业的资质
export function updateEnterpriseQualification(data) {
  return request({
    url: '/api/company-qualification/updateQualification',
    method: 'post',
    data,
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}
//删除指定企业的资质
export function deleteEnterpriseQualificationById(id) {
  return request({
    url: '/api/company-qualification/deleteQualificationById',
    method: 'get',
    params: {
      id
    }
  })
}
