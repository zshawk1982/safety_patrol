/*
 * @Author: your name
 * @Date: 2020-04-27 17:55:36
 * @LastEditTime: 2020-05-05 15:03:55
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\checkplan.js
 */
import request from '@/utils/request'

export function fetchList(query) {
  return request({
    // url: '/vue-element-admin/checkplan/list',//
    url: '/api/check-plan/getListByPaged', //    
    method: 'get',
    params: query
  })
}
export function fetchBasics() {
  return request({
    url: '/api/check-plan/add', //获取基础的add 下来等消息    
    method: 'get',
  })
}
// 中止
export function fetchCancel(id) {
  return request({
    url: '/api/check-plan/cancel', //获取基础的add 下来等消息    
    method: 'get',
    params: {
      id
    }

  })
}

//删除
export function deletePlan(query) {
  return request({
    url: '/api/check-plan/delete',
    method: 'get',
    params: query
  })
}

export function fetchCheckPlan(id) { //查看
  return request({
    url: '/api/check-plan/details',
    method: 'get',
    params: {
      id
    }
  })
}

export function publishCheckPlan(query) { //xiug执行状态修改为已发布
  return request({
    url: '/api/check-plan/release',
    method: 'get',
    params:query

  })
}

export function createCheckPlan(data) {
  return request({
    url: '/api/check-plan/save',
    method: 'post',
    data
  })
}

export function updateCheckPlan(id) { //修改获取
  return request({
    url: '/api/check-plan/update', ////
    method: 'get',
    params: {
      id
    }
  })
}
