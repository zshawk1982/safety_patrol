/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-04-10 15:24:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\user.js
 */
import request from '@/utils/request'

//查询组织树信息
export function fetchTreeData() {
  return request({
    url: '/vue-element-admin/checktarget/treeData',
    method: 'get'
  })
}
//删除组织信息
export function deleteChecktarget(query) {
  return request({
    url: '/vue-element-admin/checktarget/delete',
    method: 'get',
    params: query
  })
}
//新增组织信息
export function addChecktarget(query) {
  return request({
    url: '/vue-element-admin/checktarget/add',
    method: 'get',
    params: query
  })
}


