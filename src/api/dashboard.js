/*
 * @Author: your name
 * @Date: 2020-05-05 14:46:22
 * @LastEditTime: 2020-05-05 15:20:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\dashboard.js
 */
import request from '@/utils/request'
//查询所有隐患统计信息
export function fetchTroubleAllInfo() {
  return request({
    url: '/api/sys-mainpage/dangerCount',
    method: 'get'
  })
}

//查询所有未检查任务信息
export function fetchUnaccomplishedTask() {
  return request({
    url: '/api/sys-mainpage/unaccomplishedTask',
    method: 'get'
  })
}

//查询所有已检查任务信息
export function fetchAccomplishedTask() {
  return request({
    url: '/api/sys-mainpage/accomplishedTask',
    method: 'get'
  })
}
//查询所有待复查隐患
export function fetchToBeReviewDanger() {
  return request({
    url: '/api/sys-mainpage/toBeReviewDanger',
    method: 'get'
  })
}
//查询所有已复查隐患
export function fetchUnRectifyDanger(query) {
  return request({
    url: '/api/sys-mainpage/unRectifyDanger',
    method: 'get',
    params: query
  })
}


