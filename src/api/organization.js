/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-05-19 12:44:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\user.js
 */
import request from '@/utils/request'

//查询组织树信息
export function fetchTreeData(data) {
  return request({
    url: '/api/sys-organization/getOrgTreeList',
    method: 'post',
    data
  })
}
//删除组织信息
export function deleteOrganization(data) {
  return request({
    url: '/api/sys-organization/delete',
    method: 'post',
    data
  })
}
//新增组织信息
export function addOrganization(data) {
  return request({
    url: '/api/sys-organization/add',
    method: 'post',
    data
  })
}
//修改组织信息
export function editOrganization(data) {
  return request({
    url: '/api/sys-organization/edit',
    method: 'post',
    data
  })
}
//根据组织id获取用户列表
export function fetchUserListByOrg(startId) {
  return request({
    url: '/api/sys-organization/getOrgUserList',
    method: 'post',
    data: { startId }
  })
}

