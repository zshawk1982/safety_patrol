/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-06-22 16:53:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\api\user.js
 */
import request from '@/utils/request'

export function login(data) {
  return request({
    // url: '/vue-element-admin/user/login',
    url: '/api/sys/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/api/sys/userInfo',
    // url: '/vue-element-admin/user/info',
    method: 'get',
    // params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}

export function fetchList(data) {
  return request({
    url: '/api/sys/userList',
    method: 'post',
    data
  })
}

export function doubleUserAccount(data) {
  return request({
    url: '/api/sys/doubleUserAccount',
    method: 'post',
    data
  })
}
//查询消息用户对象
export function searchUser(data) {
  return request({
    url: '/api/sys/userList',
    method: 'post',
    data
  })
}
//禁用用户
export function deleteUser(data) {
  return request({
    url: '/api/sys/delete',
    method: 'post',
    data
  })
}
//新增用户
export function addUser(data) {
  return request({
    url: '/api/sys/addUser',
    method: 'post',
    data
  })
}
//编辑用户
export function editUser(data) {
  return request({
    url: '/api/sys/editUser',
    method: 'post',
    data
  })
}
//根据用户id获取用户详细信息
export function fetchUserDataById(id) {
  return request({
    url: '/api/sys/getUserDetailInfo',
    method: 'get',
    params: { userId: id },
  })
}

//修改用户密码
export function editUserPassword(data) {
  return request({
    url: '/api/sys/editPassword',
    method: 'post',
    data
  })
}
//启用用户
export function enableUser(data) {
  return request({
    url: '/api/sys/reuse',
    method: 'post',
    data
  })
}


