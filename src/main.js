/*
 * @Author: your name
 * @Date: 2020-01-27 17:43:51
 * @LastEditTime: 2020-05-19 14:00:13
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-element-admin\src\main.js
 */
import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
// import './styles/element-variables.scss'
// import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/custom-theme2/index.css'
import '@/styles/index.scss' // global css



import App from './App'
import store from './store'
import router from './router'
import './icons' // icon
//引入权限控制，对router进行权限的相关处理
import './permission' // permission control
import './utils/error-log' // error log
import * as filters from './filters' // global filters
Vue.use(require('vue-moment'));
Vue.use(Element);
Vue.config.silent = true
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
