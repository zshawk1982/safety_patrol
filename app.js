/*
 * @Description: 服务器启动文件
 * @Author: your name
 * @Date: 2019-10-12 08:17:22
 * @LastEditTime: 2020-04-07 21:03:12
 * @LastEditors: Please set LastEditors
 */

const fs = require('fs')
const path = require('path')
const express = require('express')
const chalk = require('chalk')
// const compression = require('compression')
const app = express()
// app.use(compression())
// 设置允许跨域访问该服务.
// app.all('*', function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*')
//   // Access-Control-Allow-Headers ,可根据浏览器的F12查看,把对应的粘贴在这里就行
//   res.header('Access-Control-Allow-Headers', 'Content-Type')
//   res.header('Access-Control-Allow-Methods', '*')
//   res.header('Content-Type', 'application/json;charset=utf-8')
//   next()
// })
app.use(express.static(path.resolve(__dirname, './dist')))// 生产环境目录

app.get('*', function (req, res) {
  const html = fs.readFileSync(path.resolve(__dirname, './dist/index.html'), 'utf-8')// 生产环境目录index文件
  res.send(html)
})
app.listen(9090, res => { // 端口
  console.log(chalk.yellow('Start Service On 9090'))
})
